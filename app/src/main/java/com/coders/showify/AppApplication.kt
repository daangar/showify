package com.coders.showify

import android.app.Application
import com.coders.showify.data.di.dataModule
import com.coders.showify.domain.di.domainModule
import com.coders.showify.presentation.di.presentationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class AppApplication: Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@AppApplication)
            modules(appModules)
        }
    }
}

val appModules = presentationModule + domainModule + dataModule