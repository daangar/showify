package com.coders.showify.base.mvi

interface Action {
    suspend fun execute()
}