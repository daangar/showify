package com.coders.showify.base.mvi

interface ActionFactory<VIEWSTATE, VIEWCOMMAND, VIEWINTENT, SCREENINTENT: VIEWINTENT,
        VIEWMODEL: MVIViewModel<VIEWSTATE, VIEWCOMMAND, VIEWINTENT>> {

    fun fromIntent(intent: SCREENINTENT, viewModel: VIEWMODEL): Action
}