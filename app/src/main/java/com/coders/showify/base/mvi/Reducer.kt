package com.coders.showify.base.mvi

interface Reducer<VIEWSTATE> {
    suspend fun reduce(viewState: VIEWSTATE): VIEWSTATE
}