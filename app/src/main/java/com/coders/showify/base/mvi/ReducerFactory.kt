package com.coders.showify.base.mvi

interface ReducerFactory<REDUCERINTENT, VIEWSTATE> {
    fun fromIntent(intent: REDUCERINTENT): Reducer<VIEWSTATE>
}