package com.coders.showify.base.mvi

sealed class ViewIntent {
    sealed class Screen {}
    sealed class Reducer {}
}