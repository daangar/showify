package com.coders.showify.data.di

import com.coders.showify.data.repository.EpisodeRepositoryImpl
import com.coders.showify.data.repository.SeasonRepositoryImpl
import com.coders.showify.data.repository.ShowRepositoryImpl
import com.coders.showify.domain.repository.EpisodeRepository
import com.coders.showify.domain.repository.SeasonRepository
import com.coders.showify.domain.repository.ShowRepository
import org.koin.dsl.module

val repositoryModule = module {
    factory<ShowRepository> { ShowRepositoryImpl(get()) }
    factory<SeasonRepository> { SeasonRepositoryImpl(get()) }
    factory<EpisodeRepository> { EpisodeRepositoryImpl(get()) }
}