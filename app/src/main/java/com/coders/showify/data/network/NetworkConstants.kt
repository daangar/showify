package com.coders.showify.data.network

object NetworkConstants {
    const val BASE_URL = "https://api.tvmaze.com/"
    object Endpoints {
        const val SHOWS = "shows"
        const val SEARCH = "search/shows"
        const val SEASONS = "shows/{id}/seasons"
        const val EPISODES = "seasons/{id}/episodes"
    }
}