package com.coders.showify.data.network

import com.coders.showify.data.network.response.EpisodeResponse
import com.coders.showify.data.network.response.SearchResultResponse
import com.coders.showify.data.network.response.SeasonResponse
import com.coders.showify.data.network.response.ShowResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TVMazeService {
    @GET(NetworkConstants.Endpoints.SHOWS)
    suspend fun getShows(@Query("page") page: Int = 0): Response<List<ShowResponse>>

    @GET(NetworkConstants.Endpoints.SEARCH)
    suspend fun searchShows(@Query("q") query: String): Response<List<SearchResultResponse>>

    @GET(NetworkConstants.Endpoints.SEASONS)
    suspend fun getSeasonsByShow(@Path("id") id: Long): Response<List<SeasonResponse>>

    @GET(NetworkConstants.Endpoints.EPISODES)
    suspend fun getEpisodesbySeason(@Path("id") id: Long): Response<List<EpisodeResponse>>
}