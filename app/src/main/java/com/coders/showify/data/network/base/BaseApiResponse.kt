package com.coders.showify.data.network.base

import com.coders.showify.domain.dto.Result
import retrofit2.Response

abstract class BaseApiResponse {

    suspend fun <T: DataConverter<U>, U> safeApiCall(apiCall: suspend () -> Response<T>): Result<U> {
        try {
            val response = apiCall()
            if (response.isSuccessful) {
                val body = response.body()
                body?.let {
                    return Result.Success(body.convert())
                }
            }
            return failure("${response.code()} ${response.message()}")
        } catch (e: Exception) {
            return failure(e.message ?: e.toString())
        }
    }

    suspend fun <T: DataConverter<U>, U> safeApiCallList(apiCall: suspend () -> Response<List<T>>): Result<List<U>> {
        try {
            val response = apiCall()
            if (response.isSuccessful) {
                val body = response.body()
                body?.let {
                    return Result.Success(body.map { it.convert() })
                }
            }
            return failure("${response.code()} ${response.message()}")
        } catch (e: Exception) {
            return failure(e.message ?: e.toString())
        }
    }

    private fun <T> failure(errorMessage: String): Result<T> =
        Result.Failure("Api call failed $errorMessage")
}