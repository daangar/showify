package com.coders.showify.data.network.base

interface DataConverter<T> {
    fun convert(): T
}