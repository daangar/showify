package com.coders.showify.data.network.response

import com.coders.showify.data.network.base.DataConverter
import com.coders.showify.domain.model.Episode
import com.coders.showify.domain.model.Image
import com.coders.showify.domain.model.Season

data class SeasonResponse(
    val id: Long? = 0L,
    val number: Int? = 0
): DataConverter<Season> {
    override fun convert(): Season {
        return Season(
            id = this.id ?: 0L,
            number = this.number ?: 0
        )
    }
}

data class EpisodeResponse(
    val id: Long? = 0L,
    val name: String? = "",
    val season: Int? = 0,
    val number: Int? = 0,
    val summary: String? = "",
    val image: ImageResponse? = ImageResponse(),
    val runtime: Int? = 0
): DataConverter<Episode> {
    override fun convert(): Episode {
        return Episode(
            id = this.id ?: 0L,
            name = this.name.orEmpty(),
            season = this.season ?: 0,
            number = this.number ?: 0,
            summary = this.summary.orEmpty(),
            image = Image(
                medium = this.image?.medium.orEmpty(),
                original = this.image?.original.orEmpty()
            ),
            runtime = this.runtime ?: 0
        )
    }
}