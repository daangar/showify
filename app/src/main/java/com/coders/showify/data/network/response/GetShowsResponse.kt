package com.coders.showify.data.network.response

import com.coders.showify.data.network.base.DataConverter
import com.coders.showify.domain.model.Image
import com.coders.showify.domain.model.Schedule
import com.coders.showify.domain.model.Show

data class ShowResponse(
    val id: Long? = 0L,
    val name: String? = "",
    val image: ImageResponse? = ImageResponse(),
    val genres: List<String>? = emptyList(),
    val schedule: ScheduleResponse? = ScheduleResponse(),
    val summary: String? = "",
): DataConverter<Show> {
    override fun convert(): Show {
        return Show(
            id = this.id ?: 0L,
            name = this.name.orEmpty(),
            image = Image(
                medium = this.image?.medium.orEmpty(),
                original = this.image?.original.orEmpty()
            ),
            genres = this.genres.orEmpty(),
            schedule = Schedule(
                days = this.schedule?.days.orEmpty(),
                time = this.schedule?.time.orEmpty()
            ),
            summary = this.summary.orEmpty()
        )
    }
}

data class ScheduleResponse(
    val time: String? = "",
    val days: List<String>? = emptyList()
)

data class ImageResponse(
    val medium: String? = "",
    val original: String? = ""
)

data class SearchResultResponse(
    val show: ShowResponse? = ShowResponse()
): DataConverter<Show> {
    override fun convert(): Show {
        return this.show?.convert() ?: Show()
    }
}
