package com.coders.showify.data.repository

import com.coders.showify.data.network.TVMazeService
import com.coders.showify.data.network.base.BaseApiResponse
import com.coders.showify.domain.dto.Result
import com.coders.showify.domain.model.Episode
import com.coders.showify.domain.repository.EpisodeRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class EpisodeRepositoryImpl(
    private val tvMazeService: TVMazeService
): EpisodeRepository, BaseApiResponse() {
    override suspend fun getEpidosesBySeasonId(id: Long): Flow<Result<List<Episode>>> {
        return flow {
            emit(safeApiCallList { tvMazeService.getEpisodesbySeason(id) })
        }.flowOn(Dispatchers.IO)
    }
}