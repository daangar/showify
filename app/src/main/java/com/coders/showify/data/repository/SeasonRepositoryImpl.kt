package com.coders.showify.data.repository

import com.coders.showify.data.network.TVMazeService
import com.coders.showify.data.network.base.BaseApiResponse
import com.coders.showify.domain.dto.Result
import com.coders.showify.domain.model.Season
import com.coders.showify.domain.repository.SeasonRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class SeasonRepositoryImpl(
    private val tvMazeService: TVMazeService
): SeasonRepository, BaseApiResponse() {
    override suspend fun getSeasonsByShowId(id: Long): Flow<Result<List<Season>>> {
        return flow {
            emit(safeApiCallList { tvMazeService.getSeasonsByShow(id) })
        }.flowOn(Dispatchers.IO)
    }
}