package com.coders.showify.data.repository

import com.coders.showify.data.network.TVMazeService
import com.coders.showify.data.network.base.BaseApiResponse
import com.coders.showify.domain.model.Show
import com.coders.showify.domain.dto.Result
import com.coders.showify.domain.repository.ShowRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class ShowRepositoryImpl(
    private val tvMazeService: TVMazeService
): ShowRepository, BaseApiResponse() {
    override suspend fun getShows(): Flow<Result<List<Show>>> {
        return flow {
            emit(safeApiCallList { tvMazeService.getShows() })
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun searchShowsByQuery(query: String): Flow<Result<List<Show>>> {
        return flow {
            emit(safeApiCallList { tvMazeService.searchShows(query) })
        }.flowOn(Dispatchers.IO)
    }
}