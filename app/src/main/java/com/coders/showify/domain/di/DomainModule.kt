package com.coders.showify.domain.di

import com.coders.showify.domain.usecase.GetEpisodesBySeasonIdUseCaseImpl
import com.coders.showify.domain.usecase.GetSeasonsByShowIdUseCaseImpl
import com.coders.showify.domain.usecase.GetShowsUseCaseImpl
import com.coders.showify.domain.usecase.SearchShowsUseCaseImpl
import com.coders.showify.domain.usecase.base.GetEpisodesBySeasonIdUseCase
import com.coders.showify.domain.usecase.base.GetSeasonsByShowIdUseCase
import com.coders.showify.domain.usecase.base.GetShowsUseCase
import com.coders.showify.domain.usecase.base.SearchShowsUseCase
import org.koin.dsl.module

val domainModule = module {
    factory<GetShowsUseCase> { GetShowsUseCaseImpl(get()) }
    factory<SearchShowsUseCase> { SearchShowsUseCaseImpl(get()) }
    factory<GetSeasonsByShowIdUseCase> { GetSeasonsByShowIdUseCaseImpl(get(), get()) }
    factory<GetEpisodesBySeasonIdUseCase> { GetEpisodesBySeasonIdUseCaseImpl(get()) }
}