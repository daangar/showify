package com.coders.showify.domain.dto

sealed class Result<T> {
    class Success<T>(val data: T): Result<T>()
    class Failure<T>(val message: String, val data: T? = null): Result<T>()
}
