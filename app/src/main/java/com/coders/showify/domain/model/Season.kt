package com.coders.showify.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Season(
    val id: Long = 0L,
    val number: Int = 0,
    val episodes: List<Episode> = emptyList()
): Parcelable

@Parcelize
data class Episode(
    val id: Long = 0L,
    val name: String = "",
    val season: Int = 0,
    val number: Int = 0,
    val summary: String = "",
    val image: Image = Image(),
    val runtime: Int = 0
): Parcelable