package com.coders.showify.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class Show(
    val id: Long = 0L,
    val name: String = "",
    val image: Image = Image(),
    val genres: List<String> = emptyList(),
    val schedule: Schedule = Schedule(),
    val summary: String = "",
    val seasons: List<Season> = emptyList()
): Parcelable

@Parcelize
data class Schedule(
    val time: String = "",
    val days: List<String> = emptyList()
): Parcelable

@Parcelize
data class Image(
    val medium: String = "",
    val original: String = ""
): Parcelable
