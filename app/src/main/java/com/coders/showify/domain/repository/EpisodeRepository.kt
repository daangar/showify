package com.coders.showify.domain.repository

import com.coders.showify.domain.dto.Result
import com.coders.showify.domain.model.Episode
import kotlinx.coroutines.flow.Flow


interface EpisodeRepository {
    suspend fun getEpidosesBySeasonId(id: Long): Flow<Result<List<Episode>>>
}