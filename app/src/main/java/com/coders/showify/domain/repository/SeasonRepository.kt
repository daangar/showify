package com.coders.showify.domain.repository

import com.coders.showify.domain.dto.Result
import com.coders.showify.domain.model.Season
import kotlinx.coroutines.flow.Flow

interface SeasonRepository {
    suspend fun getSeasonsByShowId(id: Long): Flow<Result<List<Season>>>
}