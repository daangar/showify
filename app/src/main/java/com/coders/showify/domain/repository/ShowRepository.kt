package com.coders.showify.domain.repository

import com.coders.showify.domain.model.Show
import com.coders.showify.domain.dto.Result
import kotlinx.coroutines.flow.Flow

interface ShowRepository {
    suspend fun getShows(): Flow<Result<List<Show>>>
    suspend fun searchShowsByQuery(query: String): Flow<Result<List<Show>>>
}