package com.coders.showify.domain.usecase

import com.coders.showify.domain.repository.EpisodeRepository
import com.coders.showify.domain.usecase.base.GetEpisodesBySeasonIdUseCase

class GetEpisodesBySeasonIdUseCaseImpl(
    private val episodeRepository: EpisodeRepository
): GetEpisodesBySeasonIdUseCase {
    override suspend fun invoke(input: Long) = episodeRepository.getEpidosesBySeasonId(input)
}