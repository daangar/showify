package com.coders.showify.domain.usecase

import com.coders.showify.domain.dto.Result
import com.coders.showify.domain.model.Season
import com.coders.showify.domain.repository.EpisodeRepository
import com.coders.showify.domain.repository.SeasonRepository
import com.coders.showify.domain.usecase.base.GetSeasonsByShowIdUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.flowOf


class GetSeasonsByShowIdUseCaseImpl(
    private val seasonRepository: SeasonRepository,
    private val episodeRepository: EpisodeRepository
): GetSeasonsByShowIdUseCase {
    override suspend fun invoke(input: Long): Flow<Result<List<Season>>> {
        return when (val result = seasonRepository.getSeasonsByShowId(input).firstOrNull()) {
            is Result.Failure -> flowOf(result)
            is Result.Success -> flowOf(Result.Success(transformData(result.data)))
            else -> flowOf()
        }
    }

    private suspend fun transformData(data: List<Season>): List<Season> {
        return data.mapIndexed { index, season ->
            if (index > 0) season else getEpisodes(season)
        }
    }

    private suspend fun getEpisodes(season: Season): Season {
        return when (val result = episodeRepository.getEpidosesBySeasonId(season.id).firstOrNull()) {
            is Result.Success -> season.copy(episodes = result.data)
            else -> season
        }
    }


}