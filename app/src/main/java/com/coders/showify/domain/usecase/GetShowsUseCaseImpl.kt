package com.coders.showify.domain.usecase

import com.coders.showify.domain.dto.Result
import com.coders.showify.domain.model.Show
import com.coders.showify.domain.repository.ShowRepository
import com.coders.showify.domain.usecase.base.GetShowsUseCase
import kotlinx.coroutines.flow.Flow

class GetShowsUseCaseImpl(
    private val showsRepository: ShowRepository
): GetShowsUseCase {
    override suspend fun invoke(): Flow<Result<List<Show>>> =
        showsRepository.getShows()
}