package com.coders.showify.domain.usecase

import com.coders.showify.domain.repository.ShowRepository
import com.coders.showify.domain.usecase.base.SearchShowsUseCase

class SearchShowsUseCaseImpl(
    private val showRepository: ShowRepository
): SearchShowsUseCase {
    override suspend fun invoke(input: String) =
        showRepository.searchShowsByQuery(input)
}