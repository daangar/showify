package com.coders.showify.domain.usecase.base

import com.coders.showify.domain.model.Episode

interface GetEpisodesBySeasonIdUseCase: UseCaseWithInput<Long, List<Episode>>