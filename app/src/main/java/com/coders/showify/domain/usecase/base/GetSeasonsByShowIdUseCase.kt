package com.coders.showify.domain.usecase.base

import com.coders.showify.domain.model.Season

interface GetSeasonsByShowIdUseCase: UseCaseWithInput<Long, List<Season>>