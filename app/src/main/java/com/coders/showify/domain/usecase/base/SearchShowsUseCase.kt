package com.coders.showify.domain.usecase.base

import com.coders.showify.domain.model.Show

interface SearchShowsUseCase: UseCaseWithInput<String, List<Show>>