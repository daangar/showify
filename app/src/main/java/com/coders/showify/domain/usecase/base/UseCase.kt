package com.coders.showify.domain.usecase.base

import com.coders.showify.domain.dto.Result
import kotlinx.coroutines.flow.Flow

interface UseCase

interface UseCaseWithInput<INPUT, OUTPUT>: UseCase {
    suspend operator fun invoke(input: INPUT): Flow<Result<OUTPUT>>
}

interface UseCaseNoInput<OUTPUT>: UseCase {
    suspend operator fun invoke(): Flow<Result<OUTPUT>>
}