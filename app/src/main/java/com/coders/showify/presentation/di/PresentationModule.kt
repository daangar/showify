package com.coders.showify.presentation.di

import com.coders.showify.presentation.ui.detail.viewmodel.DetailViewModel
import com.coders.showify.presentation.ui.detail.viewmodel.factories.DetailActionFactory
import com.coders.showify.presentation.ui.detail.viewmodel.factories.DetailReducerFactory
import com.coders.showify.presentation.ui.episode.viewmodel.EpisodeViewModel
import com.coders.showify.presentation.ui.episode.viewmodel.factories.EpisodeReducerFactory
import com.coders.showify.presentation.ui.shows.viewmodel.ShowsViewModel
import com.coders.showify.presentation.ui.shows.viewmodel.factories.ShowsActionFactory
import com.coders.showify.presentation.ui.shows.viewmodel.factories.ShowsReducerFactory
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {

    //region action factories

    single { ShowsActionFactory(get(), get()) }
    single { DetailActionFactory(get(), get()) }

    //endregion

    //region reducer factories

    single { ShowsReducerFactory() }
    single { DetailReducerFactory() }
    single { EpisodeReducerFactory() }

    //endregion

    //region viewmodels

    viewModel { ShowsViewModel(get(), get()) }
    viewModel { DetailViewModel(get(), get()) }
    viewModel { EpisodeViewModel(get()) }

    //endregion
}