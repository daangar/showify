package com.coders.showify.presentation.ui.detail

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.coders.showify.R
import com.coders.showify.base.mvi.observeInLifecycle
import com.coders.showify.databinding.FragmentDetailBinding
import com.coders.showify.domain.model.Episode
import com.coders.showify.domain.model.Season
import com.coders.showify.domain.model.Show
import com.coders.showify.presentation.ui.detail.viewmodel.DetailCommand
import com.coders.showify.presentation.ui.detail.viewmodel.DetailIntent
import com.coders.showify.presentation.ui.detail.viewmodel.DetailState
import com.coders.showify.presentation.ui.detail.viewmodel.DetailViewModel
import com.coders.showify.presentation.utlis.afterItemSelected
import com.coders.showify.presentation.utlis.makeVisible
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailFragment: Fragment() {

    private val viewModel: DetailViewModel by viewModel()
    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<DetailFragmentArgs>()

    private val episodesAdapter by lazy { EpisodesAdapter(mutableListOf()) {
        findNavController().navigate(
            DetailFragmentDirections.fromDetailToEpisode(it)
        )
    }}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailBinding.inflate(inflater, container, false)
        setupUI()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.viewState.onEach { state ->
            handleState(state)
        }.observeInLifecycle(viewLifecycleOwner)

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.viewCommand.collect { command ->
                command.consume {
                    handleCommand(command.data)
                }
            }
        }

        viewModel.onIntent(DetailIntent.Reduce.ShowLoaded(args.show))
        viewModel.onIntent(DetailIntent.Screen.GetSeasons(args.show.id))
    }

    private fun handleCommand(data: DetailCommand) {

    }

    private fun handleState(state: DetailState) {
        when (state) {
            is DetailState.Loaded -> showInitialData(state.show)
            is DetailState.LoadSeasons -> loadSeasons(state.seasons)
            is DetailState.LoadEpisodes -> loadEpisodes(state.episodes)
            is DetailState.Loading -> binding.loading.makeVisible()
        }
    }

    private fun setupUI() {
        with((requireActivity() as AppCompatActivity)) {
            setSupportActionBar(binding.toolbar)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }

        with(binding.rvEpisodes) {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = episodesAdapter
        }

        binding.seasonSelector.afterItemSelected { _, seasonId ->
            viewModel.onIntent(DetailIntent.Screen.GetEpisodes(seasonId))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> findNavController().popBackStack()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showInitialData(show: Show) {
        with(binding) {
            collapsingToolbar.title = show.name
            Glide.with(requireActivity())
                .load(show.image.medium)
                .into(imgPoster)
            expandTextView.text = HtmlCompat.fromHtml(
                show.summary,
                HtmlCompat.FROM_HTML_MODE_COMPACT
            )
            textGenres.text = show.genres.joinToString(separator = ", ")
            textSchedule.text = getString(
                R.string.schedule,
                show.schedule.days.joinToString(separator = " - "),
                show.schedule.time
            )
        }
    }

    private fun loadSeasons(seasons: List<Season>) {
        binding.seasonSelector.isVisible = seasons.size > 1
        binding.seasonSelector.adapter = createSeasonsAdapter(seasons)
        seasons[0].let {
            if (it.episodes.isNotEmpty()) loadEpisodes(it.episodes)
        }
    }

    private fun createSeasonsAdapter(seasons: List<Season>): SeasonAdapter {
        return SeasonAdapter(seasons, requireContext())
    }

    private fun loadEpisodes(episodes: List<Episode>) {
        episodesAdapter.addEpisodes(episodes)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}