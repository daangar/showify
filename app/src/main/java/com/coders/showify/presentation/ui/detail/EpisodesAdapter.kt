package com.coders.showify.presentation.ui.detail

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.coders.showify.R
import com.coders.showify.databinding.ItemEpisodeBinding
import com.coders.showify.domain.model.Episode

class EpisodesAdapter(
    private val episodes: MutableList<Episode>,
    private val onClicked: (Episode) -> Unit = {}
): RecyclerView.Adapter<EpisodesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemEpisodeBinding.inflate(
            LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(episodes[position])
    }

    override fun getItemCount() = episodes.size

    @SuppressLint("NotifyDataSetChanged")
    fun addEpisodes(episodes: List<Episode>) {
        this.episodes.clear()
        this.episodes.addAll(episodes)
        this.notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ItemEpisodeBinding): RecyclerView.ViewHolder(binding.root) {

        private lateinit var item: Episode

        init {
            binding.root.setOnClickListener {
                if (::item.isInitialized) {
                    onClicked(item)
                }
            }
        }

        fun bind(episode: Episode) {
            item = episode
            with(binding) {

                Glide.with(binding.root)
                    .load(episode.image.medium)
                    .into(imgPoster)

                textTitle.text = binding.root.context.getString(
                    R.string.episode,
                    episode.number.toString(),
                    episode.name
                )
                textRuntime.text = formatRuntime(episode.runtime, root.context)
                textSummary.text = HtmlCompat.fromHtml(episode.summary, HtmlCompat.FROM_HTML_MODE_COMPACT)
            }
        }

        private fun formatRuntime(runtime: Int, context: Context): String {
            val hours = runtime / 60
            val minutes = runtime % 60
            return when {
                hours == 0 -> context.getString(R.string.runtime_minutes, runtime.toString())
                minutes == 0 -> context.getString(R.string.runtime_hours, hours.toString())
                else -> context.getString(R.string.runtime_hours_minutes, hours.toString(), minutes.toString())
            }
        }
    }
}