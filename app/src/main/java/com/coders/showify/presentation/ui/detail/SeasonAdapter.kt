package com.coders.showify.presentation.ui.detail

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.CheckedTextView
import com.coders.showify.R
import com.coders.showify.domain.model.Season

class SeasonAdapter(
    private val seasons: List<Season>,
    private val context: Context
): BaseAdapter() {

    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount() = seasons.size

    override fun getItem(position: Int) = seasons[position]

    override fun getItemId(position: Int) = seasons[position].id


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val vh: ItemHolder
        if (convertView == null) {
            view = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false)
            vh = ItemHolder(view)
            view.tag = vh
        } else {
            view = convertView
            vh = view.tag as ItemHolder
        }
        vh.checkedTextView.text = context.getString(R.string.season, seasons[position].number.toString())
        return view
    }

    inner class ItemHolder(itemView: View) {
        val checkedTextView: CheckedTextView = itemView.findViewById(android.R.id.text1)
    }
}