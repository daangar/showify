package com.coders.showify.presentation.ui.detail.viewmodel

import com.coders.showify.domain.model.Episode
import com.coders.showify.domain.model.Season
import com.coders.showify.domain.model.Show

sealed class DetailIntent {
    sealed class Screen: DetailIntent() {
        data class GetSeasons(val showId: Long): Screen()
        data class GetEpisodes(val seasonId: Long): Screen()
    }

    sealed class Reduce: DetailIntent() {
        object Loading: Reduce()
        data class ShowLoaded(
            val show: Show
        ): Reduce()
        data class AddSeasonsToShow(
            val seasons: List<Season>
        ): Reduce()
        data class ShowEpisodes(
            val episodes: List<Episode>
        ): Reduce()
    }
}
