package com.coders.showify.presentation.ui.detail.viewmodel

import com.coders.showify.domain.model.Episode
import com.coders.showify.domain.model.Season
import com.coders.showify.domain.model.Show

sealed class DetailState {
    object Loading: DetailState()
    data class Loaded(
        val show: Show
    ): DetailState()
    data class LoadSeasons(
        val seasons: List<Season>
    ): DetailState()
    data class LoadEpisodes(
        val episodes: List<Episode>
    ): DetailState()
}
