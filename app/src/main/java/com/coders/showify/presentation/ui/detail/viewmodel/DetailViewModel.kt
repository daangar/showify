package com.coders.showify.presentation.ui.detail.viewmodel

import androidx.lifecycle.viewModelScope
import com.coders.showify.base.mvi.MVIViewModel
import com.coders.showify.presentation.ui.detail.viewmodel.factories.DetailActionFactory
import com.coders.showify.presentation.ui.detail.viewmodel.factories.DetailReducerFactory
import kotlinx.coroutines.launch

class DetailViewModel(
    private val actionFactory: DetailActionFactory,
    private val reducerFactory: DetailReducerFactory
): MVIViewModel<DetailState, DetailCommand, DetailIntent>() {

    override val initialState: DetailState
        get() = DetailState.Loading

    override suspend fun handleIntent(intent: DetailIntent) {
        viewModelScope.launch {
            when(intent) {
                is DetailIntent.Reduce -> reducerFactory
                    .fromIntent(intent)
                    .reduce(currentState)
                    .let { setState(it) }
                is DetailIntent.Screen -> actionFactory
                    .fromIntent(intent, this@DetailViewModel)
                    .execute()
            }
        }
    }

}