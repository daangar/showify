package com.coders.showify.presentation.ui.detail.viewmodel.actions

import com.coders.showify.base.mvi.Action
import com.coders.showify.domain.dto.Result
import com.coders.showify.domain.model.Episode
import com.coders.showify.domain.usecase.base.GetEpisodesBySeasonIdUseCase
import com.coders.showify.presentation.ui.detail.viewmodel.DetailIntent
import com.coders.showify.presentation.ui.detail.viewmodel.DetailViewModel
import kotlinx.coroutines.flow.firstOrNull

class GetEpisodesAction(
    private val getEpisodes: GetEpisodesBySeasonIdUseCase,
    private val seasonId: Long,
    private val viewModel: DetailViewModel
): Action {
    override suspend fun execute() {
        when (val result = getEpisodes(seasonId).firstOrNull()) {
            is Result.Success -> processResult(result.data)
            is Result.Failure -> processError(result.message)
        }
    }

    private fun processError(message: String?) {

    }

    private fun processResult(episodes: List<Episode>) {
        viewModel.onIntent(DetailIntent.Reduce.ShowEpisodes(episodes))
    }
}