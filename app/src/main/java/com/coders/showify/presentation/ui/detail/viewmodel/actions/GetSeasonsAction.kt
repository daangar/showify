package com.coders.showify.presentation.ui.detail.viewmodel.actions

import com.coders.showify.base.mvi.Action
import com.coders.showify.domain.dto.Result
import com.coders.showify.domain.model.Season
import com.coders.showify.domain.usecase.base.GetSeasonsByShowIdUseCase
import com.coders.showify.presentation.ui.detail.viewmodel.DetailIntent
import com.coders.showify.presentation.ui.detail.viewmodel.DetailViewModel
import kotlinx.coroutines.flow.firstOrNull

class GetSeasonsAction (
    private val getSeasons: GetSeasonsByShowIdUseCase,
    private val showId: Long,
    private val viewModel: DetailViewModel
): Action {
    override suspend fun execute() {
        when (val result = getSeasons(showId).firstOrNull()) {
            is Result.Success -> processResult(result.data)
            is Result.Failure -> processError(result.message)
        }
    }

    private fun processError(message: String?) {

    }

    private fun processResult(data: List<Season>) {
        viewModel.onIntent(DetailIntent.Reduce.AddSeasonsToShow(data))
    }
}