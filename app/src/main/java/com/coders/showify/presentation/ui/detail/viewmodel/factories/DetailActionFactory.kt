package com.coders.showify.presentation.ui.detail.viewmodel.factories

import com.coders.showify.base.mvi.Action
import com.coders.showify.base.mvi.ActionFactory
import com.coders.showify.domain.usecase.base.GetEpisodesBySeasonIdUseCase
import com.coders.showify.domain.usecase.base.GetSeasonsByShowIdUseCase
import com.coders.showify.presentation.ui.detail.viewmodel.DetailCommand
import com.coders.showify.presentation.ui.detail.viewmodel.DetailIntent
import com.coders.showify.presentation.ui.detail.viewmodel.DetailState
import com.coders.showify.presentation.ui.detail.viewmodel.DetailViewModel
import com.coders.showify.presentation.ui.detail.viewmodel.actions.GetEpisodesAction
import com.coders.showify.presentation.ui.detail.viewmodel.actions.GetSeasonsAction


class DetailActionFactory(
    private val getSeasonsByShowIdUseCase: GetSeasonsByShowIdUseCase,
    private val getEpisodesBySeasonIdUseCase: GetEpisodesBySeasonIdUseCase
): ActionFactory<DetailState, DetailCommand, DetailIntent, DetailIntent.Screen, DetailViewModel> {
    override fun fromIntent(intent: DetailIntent.Screen, viewModel: DetailViewModel): Action {
        return when(intent) {
            is DetailIntent.Screen.GetSeasons -> GetSeasonsAction(
                getSeasons = getSeasonsByShowIdUseCase,
                showId = intent.showId,
                viewModel = viewModel
            )
            is DetailIntent.Screen.GetEpisodes -> GetEpisodesAction(
                getEpisodes = getEpisodesBySeasonIdUseCase,
                seasonId = intent.seasonId,
                viewModel = viewModel
            )
        }
    }
}