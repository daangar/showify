package com.coders.showify.presentation.ui.detail.viewmodel.factories

import com.coders.showify.base.mvi.Reducer
import com.coders.showify.base.mvi.ReducerFactory
import com.coders.showify.presentation.ui.detail.viewmodel.DetailIntent
import com.coders.showify.presentation.ui.detail.viewmodel.DetailState
import com.coders.showify.presentation.ui.detail.viewmodel.reducers.AddSeasonsReducer
import com.coders.showify.presentation.ui.detail.viewmodel.reducers.LoadEpisodesReducer
import com.coders.showify.presentation.ui.detail.viewmodel.reducers.LoadingReducer
import com.coders.showify.presentation.ui.detail.viewmodel.reducers.ShowLoadedReducer

class DetailReducerFactory: ReducerFactory<DetailIntent.Reduce, DetailState> {
    override fun fromIntent(intent: DetailIntent.Reduce): Reducer<DetailState> {
        return when (intent) {
            is DetailIntent.Reduce.ShowLoaded -> ShowLoadedReducer(intent.show)
            is DetailIntent.Reduce.AddSeasonsToShow -> AddSeasonsReducer(intent.seasons)
            is DetailIntent.Reduce.ShowEpisodes -> LoadEpisodesReducer(intent.episodes)
            is DetailIntent.Reduce.Loading -> LoadingReducer()
        }
    }
}