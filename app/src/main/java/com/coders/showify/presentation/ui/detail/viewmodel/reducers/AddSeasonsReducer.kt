package com.coders.showify.presentation.ui.detail.viewmodel.reducers

import com.coders.showify.base.mvi.Reducer
import com.coders.showify.domain.model.Season
import com.coders.showify.domain.model.Show
import com.coders.showify.presentation.ui.detail.viewmodel.DetailState

class AddSeasonsReducer(
    private val seasons: List<Season>
): Reducer<DetailState> {
    override suspend fun reduce(viewState: DetailState): DetailState {
        return DetailState.LoadSeasons(seasons)
    }
}