package com.coders.showify.presentation.ui.detail.viewmodel.reducers

import com.coders.showify.base.mvi.Reducer
import com.coders.showify.presentation.ui.detail.viewmodel.DetailState

class LoadingReducer: Reducer<DetailState> {
    override suspend fun reduce(viewState: DetailState): DetailState {
        return DetailState.Loading
    }
}