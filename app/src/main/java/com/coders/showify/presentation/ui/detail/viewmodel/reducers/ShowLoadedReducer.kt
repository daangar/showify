package com.coders.showify.presentation.ui.detail.viewmodel.reducers

import com.coders.showify.base.mvi.Reducer
import com.coders.showify.domain.model.Show
import com.coders.showify.presentation.ui.detail.viewmodel.DetailState

class ShowLoadedReducer(
    private val show: Show
): Reducer<DetailState> {
    override suspend fun reduce(viewState: DetailState): DetailState {
        return DetailState.Loaded(show)
    }
}