package com.coders.showify.presentation.ui.episode

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.coders.showify.R
import com.coders.showify.base.mvi.observeInLifecycle
import com.coders.showify.databinding.FragmentEpisodeBinding
import com.coders.showify.domain.model.Episode
import com.coders.showify.presentation.ui.episode.viewmodel.EpisodeCommand
import com.coders.showify.presentation.ui.episode.viewmodel.EpisodeIntent
import com.coders.showify.presentation.ui.episode.viewmodel.EpisodeState
import com.coders.showify.presentation.ui.episode.viewmodel.EpisodeViewModel
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class EpisodeFragment: Fragment() {
    private val viewModel: EpisodeViewModel by viewModel()
    private var _binding: FragmentEpisodeBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<EpisodeFragmentArgs>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        viewModel.onIntent(EpisodeIntent.Reduce.LoadEpisode(args.episode))
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentEpisodeBinding.inflate(inflater, container, false)
        setupUI()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.viewState.onEach { state ->
            handleState(state)
        }.observeInLifecycle(viewLifecycleOwner)

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.viewCommand.collect { command ->
                command.consume {
                    handleCommand(command.data)
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> findNavController().popBackStack()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun handleCommand(data: EpisodeCommand) {

    }

    private fun handleState(state: EpisodeState) {
        when (state) {
            is EpisodeState.LoadedEpisode -> showInitialData(state.episode)
            else -> {}
        }
    }

    private fun setupUI() {
        with((requireActivity() as AppCompatActivity)) {
            setSupportActionBar(binding.toolbar)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
    }

    private fun showInitialData(episode: Episode) {
        with(binding) {
            collapsingToolbar.title = episode.name
            Glide.with(requireActivity())
                .load(episode.image.medium)
                .into(imgPoster)
            textSummary.text = HtmlCompat.fromHtml(
                episode.summary,
                HtmlCompat.FROM_HTML_MODE_COMPACT
            )
            textSeason.text = getString(
                R.string.season_number,
                episode.season.toString()
            )
            textNumber.text = getString(
                R.string.episode_number,
                episode.number.toString()
            )
        }
    }
}