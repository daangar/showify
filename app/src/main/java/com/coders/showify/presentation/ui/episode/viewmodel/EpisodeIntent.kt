package com.coders.showify.presentation.ui.episode.viewmodel

import com.coders.showify.domain.model.Episode

sealed class EpisodeIntent {

    sealed class Reduce: EpisodeIntent() {
        data class LoadEpisode(
            val episode: Episode
        ): Reduce()
    }
}
