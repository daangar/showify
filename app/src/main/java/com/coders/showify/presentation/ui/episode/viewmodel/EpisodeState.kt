package com.coders.showify.presentation.ui.episode.viewmodel

import com.coders.showify.domain.model.Episode

sealed class EpisodeState {
    object Loading: EpisodeState()
    data class LoadedEpisode(
        val episode: Episode
    ): EpisodeState()
}