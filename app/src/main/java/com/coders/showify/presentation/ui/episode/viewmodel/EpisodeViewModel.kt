package com.coders.showify.presentation.ui.episode.viewmodel

import androidx.lifecycle.viewModelScope
import com.coders.showify.base.mvi.MVIViewModel
import com.coders.showify.presentation.ui.episode.viewmodel.factories.EpisodeReducerFactory
import kotlinx.coroutines.launch

class EpisodeViewModel(
    private val reducerFactory: EpisodeReducerFactory
): MVIViewModel<EpisodeState, EpisodeCommand, EpisodeIntent>() {
    override val initialState: EpisodeState
        get() = EpisodeState.Loading

    override suspend fun handleIntent(intent: EpisodeIntent) {
        viewModelScope.launch {
            when (intent) {
                is EpisodeIntent.Reduce -> reducerFactory
                    .fromIntent(intent)
                    .reduce(currentState)
                    .let { setState(it) }
            }
        }
    }
}