package com.coders.showify.presentation.ui.episode.viewmodel.factories

import com.coders.showify.base.mvi.Reducer
import com.coders.showify.base.mvi.ReducerFactory
import com.coders.showify.presentation.ui.episode.viewmodel.EpisodeIntent
import com.coders.showify.presentation.ui.episode.viewmodel.EpisodeState
import com.coders.showify.presentation.ui.episode.viewmodel.reducers.LoadEpisodeReducer

class EpisodeReducerFactory: ReducerFactory<EpisodeIntent, EpisodeState> {
    override fun fromIntent(intent: EpisodeIntent): Reducer<EpisodeState> {
        return when (intent) {
            is EpisodeIntent.Reduce.LoadEpisode -> LoadEpisodeReducer(intent.episode)
        }
    }
}