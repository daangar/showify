package com.coders.showify.presentation.ui.episode.viewmodel.reducers

import com.coders.showify.base.mvi.Reducer
import com.coders.showify.domain.model.Episode
import com.coders.showify.presentation.ui.episode.viewmodel.EpisodeState

class LoadEpisodeReducer(
    private val episode: Episode
): Reducer<EpisodeState> {
    override suspend fun reduce(viewState: EpisodeState): EpisodeState {
        return EpisodeState.LoadedEpisode(episode)
    }
}