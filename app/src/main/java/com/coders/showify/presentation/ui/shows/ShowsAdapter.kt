package com.coders.showify.presentation.ui.shows

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.coders.showify.databinding.ItemShowBinding
import com.coders.showify.domain.model.Show

class ShowsAdapter(
    private val shows: MutableList<Show>,
    private val clickAction: (Show) -> Unit = {}
): RecyclerView.Adapter<ShowsAdapter.GridViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GridViewHolder {
        val binding = ItemShowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return GridViewHolder(binding)
    }

    override fun onBindViewHolder(holder: GridViewHolder, position: Int) {
        val show = shows[position]
        holder.bind(show)
    }

    override fun getItemCount() = shows.size

    @SuppressLint("NotifyDataSetChanged")
    fun addShows(shows: List<Show>) {
        this.shows.clear()
        this.shows.addAll(shows)
        this.notifyDataSetChanged()
    }

    inner class GridViewHolder(private val binding: ItemShowBinding): RecyclerView.ViewHolder(binding.root) {
        private lateinit var show: Show
        init {
            binding.root.setOnClickListener {
                if (::show.isInitialized) clickAction(show)
            }
        }
        fun bind(item: Show) {
            this.show = item
            Glide.with(binding.root).load(item.image.medium).into(binding.imgPoster)
            binding.textName.text = item.name
        }
    }
}