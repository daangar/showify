package com.coders.showify.presentation.ui.shows

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.coders.showify.base.mvi.observeInLifecycle
import com.coders.showify.databinding.FragmentShowsBinding
import com.coders.showify.domain.model.Show
import com.coders.showify.presentation.ui.shows.viewmodel.ShowsCommand
import com.coders.showify.presentation.ui.shows.viewmodel.ShowsIntent
import com.coders.showify.presentation.ui.shows.viewmodel.ShowsState
import com.coders.showify.presentation.ui.shows.viewmodel.ShowsViewModel
import com.coders.showify.presentation.utlis.afterTextChanged
import com.coders.showify.presentation.utlis.makeInvisible
import com.coders.showify.presentation.utlis.makeVisible
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class ShowsFragment: Fragment() {
    private val viewModel: ShowsViewModel by viewModel()
    private var _binding: FragmentShowsBinding? = null
    private val binding get() = _binding!!

    private val showsAdapter by lazy { ShowsAdapter(mutableListOf()) {
        findNavController().navigate(
            ShowsFragmentDirections.fromShowsToDetail(it)
        )
    }}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onIntent(ShowsIntent.Screen.GetShows)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentShowsBinding.inflate(inflater, container, false)
        setupUI()
        return binding.root
    }

    private fun setupUI() {

        binding.rvShows.adapter = showsAdapter
        binding.rvShows.layoutManager = GridLayoutManager(requireContext(),3)

        binding.textSearch.afterTextChanged {
            viewModel.onIntent(ShowsIntent.Screen.SearchShows(it))
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.viewState.onEach { state ->
            handleState(state)
        }.observeInLifecycle(viewLifecycleOwner)

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.viewCommand.collect { command ->
                command.consume {
                    showsCommandHandler(command.data)
                }
            }
        }
    }

    private fun handleState(state: ShowsState) {
        when (state) {
            is ShowsState.Loaded -> listShows(state.shows)
            is ShowsState.Loading -> binding.loding.makeVisible()
        }
    }

    private fun listShows(shows: List<Show>) {
        binding.loding.makeInvisible()
        showsAdapter.addShows(shows)
    }

    private fun showsCommandHandler(data: ShowsCommand) {

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}