package com.coders.showify.presentation.ui.shows.viewmodel

sealed class ShowsCommand {
    data class ShowErrorDialog(val errorMessage: String?): ShowsCommand()
    data class GoToDetailsScreen(val id: Long): ShowsCommand()
}
