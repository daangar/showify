package com.coders.showify.presentation.ui.shows.viewmodel

import com.coders.showify.domain.model.Show

sealed class ShowsIntent {
    sealed class Screen: ShowsIntent() {
        object GetShows: Screen()
        data class SearchShows(val query: String): Screen()
    }

    sealed class Reduce: ShowsIntent() {
        data class ShowsLoaded(
            val shows: List<Show>
        ): Reduce()
    }
}

