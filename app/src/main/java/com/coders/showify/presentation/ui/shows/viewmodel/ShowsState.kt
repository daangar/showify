package com.coders.showify.presentation.ui.shows.viewmodel

import com.coders.showify.domain.model.Show

sealed class ShowsState {
    object Loading : ShowsState()
    class Loaded (
        val shows: List<Show>
    ): ShowsState()
}
