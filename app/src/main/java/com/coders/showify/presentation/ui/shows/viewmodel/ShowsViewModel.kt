package com.coders.showify.presentation.ui.shows.viewmodel

import androidx.lifecycle.viewModelScope
import com.coders.showify.base.mvi.MVIViewModel
import com.coders.showify.presentation.ui.shows.viewmodel.factories.ShowsActionFactory
import com.coders.showify.presentation.ui.shows.viewmodel.factories.ShowsReducerFactory
import kotlinx.coroutines.launch

class ShowsViewModel (
    private val showsActionFactory: ShowsActionFactory,
    private val showsReducerFactory: ShowsReducerFactory
): MVIViewModel<ShowsState, ShowsCommand, ShowsIntent>() {

    override val initialState: ShowsState
        get() = ShowsState.Loading

    override suspend fun handleIntent(intent: ShowsIntent) {
        viewModelScope.launch {
            when(intent) {
                is ShowsIntent.Reduce -> showsReducerFactory
                    .fromIntent(intent)
                    .reduce(currentState)
                    .let { setState(it) }
                is ShowsIntent.Screen -> showsActionFactory
                    .fromIntent(intent, this@ShowsViewModel)
                    .execute()
            }
        }
    }
}