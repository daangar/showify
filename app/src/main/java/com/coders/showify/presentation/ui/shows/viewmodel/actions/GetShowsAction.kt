package com.coders.showify.presentation.ui.shows.viewmodel.actions

import com.coders.showify.base.mvi.Action
import com.coders.showify.domain.dto.Result
import com.coders.showify.domain.model.Show
import com.coders.showify.domain.usecase.base.GetShowsUseCase
import com.coders.showify.presentation.ui.shows.viewmodel.ShowsCommand
import com.coders.showify.presentation.ui.shows.viewmodel.ShowsIntent
import com.coders.showify.presentation.ui.shows.viewmodel.ShowsViewModel
import kotlinx.coroutines.flow.firstOrNull

class GetShowsAction(
    private val getShowsUseCase: GetShowsUseCase,
    private val viewModel: ShowsViewModel
): Action {

    override suspend fun execute() {
        when(val result = getShowsUseCase().firstOrNull()) {
            is Result.Success -> processResult(result.data)
            is Result.Failure -> processError(result.message)
            else -> {}
        }
    }

    private fun processError(message: String?) {
        viewModel.sendCommand(ShowsCommand.ShowErrorDialog(message))
    }

    private fun processResult(data: List<Show>) {
        viewModel.onIntent(ShowsIntent.Reduce.ShowsLoaded(data))
    }
}