package com.coders.showify.presentation.ui.shows.viewmodel.actions

import android.util.Log
import com.coders.showify.base.mvi.Action
import com.coders.showify.domain.dto.Result
import com.coders.showify.domain.model.Show
import com.coders.showify.domain.usecase.base.SearchShowsUseCase
import com.coders.showify.presentation.ui.shows.viewmodel.ShowsCommand
import com.coders.showify.presentation.ui.shows.viewmodel.ShowsIntent
import com.coders.showify.presentation.ui.shows.viewmodel.ShowsViewModel
import kotlinx.coroutines.flow.firstOrNull

class SearchShowsAction(
    private val searchShowsUseCase: SearchShowsUseCase,
    private val viewModel: ShowsViewModel,
    private val query: String
): Action {
    override suspend fun execute() {
        if (query.isBlank()) {
            viewModel.onIntent(ShowsIntent.Screen.GetShows)
            return
        } else if (query.length < 3) {
            viewModel.onIntent(ShowsIntent.Reduce.ShowsLoaded(emptyList()))
            return
        }
        when(val result = searchShowsUseCase(query).firstOrNull()) {
            is Result.Success -> processResult(result.data)
            is Result.Failure -> processError(result.message)
            else -> {
                Log.i("SearchShowsAction", "Aqui")
            }
        }
    }

    private fun processError(message: String?) {
        Log.i("SearchShowsAction", message.toString())
        viewModel.sendCommand(ShowsCommand.ShowErrorDialog(message))
    }

    private fun processResult(data: List<Show>) {
        Log.i("SearchShowsAction", data.toString())
        viewModel.onIntent(ShowsIntent.Reduce.ShowsLoaded(data))
    }
}