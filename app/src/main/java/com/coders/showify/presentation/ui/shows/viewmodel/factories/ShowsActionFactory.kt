package com.coders.showify.presentation.ui.shows.viewmodel.factories

import com.coders.showify.base.mvi.Action
import com.coders.showify.base.mvi.ActionFactory
import com.coders.showify.domain.usecase.base.GetShowsUseCase
import com.coders.showify.domain.usecase.base.SearchShowsUseCase
import com.coders.showify.presentation.ui.shows.viewmodel.ShowsCommand
import com.coders.showify.presentation.ui.shows.viewmodel.ShowsIntent
import com.coders.showify.presentation.ui.shows.viewmodel.ShowsState
import com.coders.showify.presentation.ui.shows.viewmodel.ShowsViewModel
import com.coders.showify.presentation.ui.shows.viewmodel.actions.GetShowsAction
import com.coders.showify.presentation.ui.shows.viewmodel.actions.SearchShowsAction

class ShowsActionFactory(
    private val getShowsUseCase: GetShowsUseCase,
    private val searchShowsUseCase: SearchShowsUseCase
): ActionFactory<ShowsState, ShowsCommand, ShowsIntent, ShowsIntent.Screen, ShowsViewModel> {
    override fun fromIntent(intent: ShowsIntent.Screen, viewModel: ShowsViewModel): Action {
        return when (intent) {
            is ShowsIntent.Screen.GetShows -> GetShowsAction(getShowsUseCase, viewModel)
            is ShowsIntent.Screen.SearchShows -> SearchShowsAction(searchShowsUseCase, viewModel, intent.query)
        }
    }
}