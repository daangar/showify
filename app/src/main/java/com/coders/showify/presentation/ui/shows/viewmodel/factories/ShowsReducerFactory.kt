package com.coders.showify.presentation.ui.shows.viewmodel.factories

import com.coders.showify.base.mvi.Reducer
import com.coders.showify.base.mvi.ReducerFactory
import com.coders.showify.presentation.ui.shows.viewmodel.ShowsIntent
import com.coders.showify.presentation.ui.shows.viewmodel.ShowsState
import com.coders.showify.presentation.ui.shows.viewmodel.reducers.ShowsLoadedReducer

class ShowsReducerFactory: ReducerFactory<ShowsIntent.Reduce, ShowsState> {
    override fun fromIntent(intent: ShowsIntent.Reduce): Reducer<ShowsState> {
        return when(intent) {
            is ShowsIntent.Reduce.ShowsLoaded -> ShowsLoadedReducer(intent.shows)
        }
    }
}