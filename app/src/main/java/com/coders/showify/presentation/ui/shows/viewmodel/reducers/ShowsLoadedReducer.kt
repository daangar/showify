package com.coders.showify.presentation.ui.shows.viewmodel.reducers

import com.coders.showify.base.mvi.Reducer
import com.coders.showify.domain.model.Show
import com.coders.showify.presentation.ui.shows.viewmodel.ShowsState

class ShowsLoadedReducer(
    private val shows: List<Show>
): Reducer<ShowsState> {
    override suspend fun reduce(viewState: ShowsState): ShowsState {
        return ShowsState.Loaded(shows)
    }
}