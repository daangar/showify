package com.coders.showify.presentation.utlis

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Spinner
import androidx.core.view.isVisible

fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable) {
            afterTextChanged.invoke(editable.toString())
        }
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        }
    })
}

fun Spinner.afterItemSelected(afterItemSelected: (Int, Long) -> Unit) {
    this.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, id: Long) {
            afterItemSelected(position, id)
        }
        override fun onNothingSelected(p0: AdapterView<*>?) {}
    }
}

fun View.makeVisible() {
    this.isVisible = true
}

fun View.makeInvisible() {
    this.isVisible = false
}