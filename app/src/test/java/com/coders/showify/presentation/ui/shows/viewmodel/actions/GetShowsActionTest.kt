package com.coders.showify.presentation.ui.shows.viewmodel.actions

import com.coders.showify.domain.dto.Result
import com.coders.showify.domain.model.Show
import com.coders.showify.domain.usecase.base.GetShowsUseCase
import com.coders.showify.presentation.ui.shows.viewmodel.ShowsIntent
import com.coders.showify.presentation.ui.shows.viewmodel.ShowsViewModel
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import io.mockk.justRun
import io.mockk.mockk
import io.mockk.slot
import kotlin.test.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest

import org.junit.Before
import org.junit.Test

class GetShowsActionTest {

    @MockK
    lateinit var viewModel: ShowsViewModel

    @Before
    fun setup() {
        MockKAnnotations.init(this)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `get shows should send the correct reducer`() = runTest {

        val listShows = listOf(
            Show(id = 1L, name = "Under the dome"),
            Show(id = 2L, name = "The Flash"),
            Show(id = 3L, name = "Arrow")
        )

        val getShowsUseCase = mockk<GetShowsUseCase>()
        coEvery { getShowsUseCase.invoke() } returns flowOf(Result.Success(listShows))

        justRun { viewModel.onIntent(any()) }

        GetShowsAction(getShowsUseCase, viewModel).execute()

        val slot = slot<ShowsIntent.Reduce.ShowsLoaded>()

        coVerify { viewModel.onIntent(capture(slot)) }

        assertEquals(listShows, slot.captured.shows)
    }
}