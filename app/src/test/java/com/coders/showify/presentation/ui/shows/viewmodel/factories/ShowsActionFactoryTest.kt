package com.coders.showify.presentation.ui.shows.viewmodel.factories

import com.coders.showify.domain.usecase.base.GetShowsUseCase
import com.coders.showify.domain.usecase.base.SearchShowsUseCase
import com.coders.showify.presentation.ui.shows.viewmodel.ShowsIntent
import com.coders.showify.presentation.ui.shows.viewmodel.actions.GetShowsAction
import com.coders.showify.presentation.ui.shows.viewmodel.actions.SearchShowsAction
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlin.test.assertTrue
import org.junit.Before
import org.junit.Test
import org.koin.test.KoinTest

class ShowsActionFactoryTest: KoinTest {

    @MockK
    lateinit var getShowsUseCase: GetShowsUseCase

    @MockK
    lateinit var searchShowsUseCase: SearchShowsUseCase

    @InjectMockKs
    lateinit var showsActionFactory: ShowsActionFactory

    @Before
    fun setup() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `when intent is GetShows action should be GetShowsAction`() {
        val action = showsActionFactory.fromIntent(ShowsIntent.Screen.GetShows, mockk())
        assertTrue(action is GetShowsAction)
    }

    @Test
    fun `when intent is SearchShows action should be SearchShowsAction`() {
        val action = showsActionFactory.fromIntent(ShowsIntent.Screen.SearchShows(""), mockk())
        assertTrue(action is SearchShowsAction)
    }
}